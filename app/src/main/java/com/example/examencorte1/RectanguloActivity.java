package com.example.examencorte1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class RectanguloActivity extends AppCompatActivity {

    private TextView txtUsuario, txtArea, txtPerimetro;
    private EditText txtBase, txtAltura;
    private Button btnCalcular, btnCerrar, btnLimpiar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_rectangulo);
        iniciarComponentes();
        //Recibir usuario
        Intent intent = getIntent();
        String usuario = intent.getStringExtra("usuario");
        //Asignar el texto al Textview
        if (usuario != null) {
            txtUsuario.setText(usuario);
        } else {
            Toast.makeText(getApplicationContext(), "No se recibió el usuario", Toast.LENGTH_SHORT).show();
        }
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtBase.getText().toString().matches("") ||
                        txtAltura.getText().toString().matches("") ){
                    Toast.makeText(getApplicationContext(),"FALTAN AGREGAR DATOS", Toast.LENGTH_SHORT).show();
                }
                else{
                    calcular();
                }
            }
        });
//gg
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                txtArea.setText("");
                txtPerimetro.setText("");
            }
        });
    }

    public void iniciarComponentes(){
        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        txtArea = (TextView) findViewById(R.id.txtArea);
        txtPerimetro = (TextView) findViewById(R.id.txtPerimetro);

        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
    }

    private void calcular(){
        int base = Integer.parseInt(txtBase.getText().toString());
        int altura = Integer.parseInt(txtAltura.getText().toString());

        //Uso de la clase Rectangulo
        Rectangulo rec = new Rectangulo(base,altura);
        float area = rec.calcularArea();
        float perimetro = rec.calcularPerimetro();

        txtArea.setText(String.valueOf(area));
        txtPerimetro.setText(String.valueOf(perimetro));
    }
}